#/bin/bash

lxsession &
picom &
nitrogen --restore &
/usr/bin/emacs --daemon &
volumeicon &
nm-applet &

export LC_CTYPE="en_GB.utf8"
export LC_CTYPE="C"
