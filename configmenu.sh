#/bin/bash

export LC_CTYPE="en_GB.utf8"
export LC_CTYPE="C"

selected=$(ls "$HOME/.config" | rofi -dmenu -p "Config")
[[ -z $selected ]] && exit

emacs $HOME/.config/$selected
